package pl.air.bookstore.service;

import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pl.air.bookstore.database.BookstoreDatabase;
import pl.air.bookstore.model.Author;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.model.Order;
import pl.air.bookstore.model.OrderLine;
import pl.air.bookstore.model.Publisher;

public class MenuService {

	private PrintStream out;
	private InputStream in;
	private PrintService printSrv;
	private BookstoreDatabase db;
	private Scanner scanIn;
	
	public MenuService(PrintStream out, InputStream in, PrintService printSrv, BookstoreDatabase db)
	{
		this.out = out;
		this.in = in;
		this.printSrv = printSrv;
		this.db = db;
	}
	
	
	//Primary methods running the system
	public void run()
	{
		printCommands();
		scanIn = new Scanner(in);
		boolean loop = true;
		while(loop)
		{
			String cmd = scanIn.next();
			switch(cmd)
			{
			case "pab":
				//print all books
				printSrv.printAllBooks(db.getAllBooks());
				break;
			case "paa":
				//print all authors;
				printSrv.printAllAuthors(db.getAllAuthors());
				break;
			case "pap":
				printSrv.printAllPublishers(db.getAllPublishers());
				break;
			case "pac":
				printSrv.printAllCustomers(db.getAllCustomers());
				break;
			case "pao":
				printSrv.printAllOrders(db.getAllOrders());
				break;
			case "ap":
				addPublisherCmd();
				break;
			case "aa":
				addAuthorCmd();	
				break;
			case "ab":
				addBookCmd();
				break;
			case "ao":
				addOrderCmd();
				break;
			case "exit":
				loop = false;
				out.println("Thank you for using Menu Service");
				break;
			}
		}
		scanIn.close();
	}
	
	private void addAuthorCmd()
	{
		String firstName = scanIn.next();
		String lastName = scanIn.next();
		Author a = new Author(firstName, lastName);
		db.addAuthor(a);
	}
	
	private void addBookCmd()
	{
		String title = null;
		if(scanIn.hasNext("\".*"))//w scannerze pierwszym znakiem jest cudzyslow, a .* oznacza �e bedzie tam wiele liter
		{
			scanIn.useDelimiter("\"");
			scanIn.next(); //odczytanie pierwszego cudzyslowa
			title = scanIn.next();
			scanIn.reset(); //przywracamy scanner do spacji
			scanIn.next(); //odczytujemy spacje za tytulem, przed rokiem wydania
		}else
		{
		 title = scanIn.next();
		}
		
		int pubYear = scanIn.nextInt();
		long publisherId = scanIn.nextLong();
		
		Book book = new Book(title, pubYear);
		
		List<Long> authorIds = new ArrayList<>();
		while(scanIn.hasNextLong())
		{
			long authorId = scanIn.nextLong();
			authorIds.add(authorId);
			Author author = db.getAuthor(authorId);
			book.addAuthor(author);
		}
		
		
		Publisher publisher = db.getPublisher(publisherId);
		book.setPublisher(publisher);
		
		db.addBook(book);
	}
	
	private void addPublisherCmd()
	{
		String name = scanIn.next();
		
		Publisher publisher = new Publisher(name);
		db.addPublisher(publisher);
		
	}
	private void addOrderCmd()
	{
		//tytul ksiazki, ilosc egzemplarzy, nazwa uzytkownika
		String title = scanIn.next();
		int quantity = scanIn.nextInt();
		String userName = scanIn.next();
		//long id = scanIn.nextLong();
		
		int seqNo = 1;
		
		//sprawdzenie czy taki uzytkownik istnieje
		if(db.getAllCustomers().contains(db.getCustomer(userName)))
		{
			//wartosc poczatkowa orderNo
			String orderNo = "...";
			Order order = new Order(orderNo, db.getCustomer(userName));
			
			orderNo = fromDateToStr(order.getOrderDateTime(), order);
			order.setOrderNo(order.getId()+ "." + orderNo);
			
			order.addOrderLine(new OrderLine(seqNo,db.getBook(title),quantity));
			seqNo++;
			
			db.addOrder(order);
		}else
			out.println("Wrong Username.");
	}
	
	//konwersja z LocalDateTime do String
	private String fromDateToStr(LocalDateTime ldt, Order order)
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime dateTime = order.getOrderDateTime();
		String stringDateTime = dateTime.format(formatter);
		
		return stringDateTime;
	}
	private void printCommands()
	{
		out.println("print all books --> pab");
		out.println("print all authors --> paa");
		out.println("print all publishers --> pap");
		out.println("print all customers --> pac");
		out.println("print all orders --> pao");
		out.println("add publisher -> ap name");
		out.println("add author --> aa first_name last_name");
		out.println("add book --> ab \"title\" pubYear publisher authorId");
		out.println("add order --> ao \"book-title\" No.-of-copies \"last_name_customer\"");
		out.println("end program --> exit");
	}
	
}
