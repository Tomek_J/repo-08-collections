package pl.air.bookstore.service;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

import pl.air.bookstore.model.Author;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.model.Customer;
import pl.air.bookstore.model.Order;
import pl.air.bookstore.model.OrderLine;
import pl.air.bookstore.model.Publisher;

public class PrintService {
	
	private PrintStream output;
	
	public PrintService(PrintStream output) {
		this.output = output;
	}

	
	// Printing Order
	public void printOrder(Order order) {
		output.println(order.getOrderNo() + "  -->  " + order.getCustomer().getLastName());
		
		SortedSet<OrderLine> orderLines = order.getOrderLines();
		for (OrderLine ordLn : orderLines) {
			printOrderLine(ordLn);
		}
		output.println();
	}
	
	// ******************* Homework - start *******************
	public void printAllOrders(Collection<Order> orders) {
		for (Order o : orders) {
			printOrder(o);
		}
	}
	// ******************* Homework - end *******************
	
	public void printOrderLine(OrderLine orderLine) {
		output.println(
			orderLine.getSeqNo() + ". " + orderLine.getBook().getTitle() + " --> " + orderLine.getQuantity() + " szt."
		);
	}
	

	// Printing Book
	public void printBook(Book book) {
		output.println(book.getTitle() + ", " + book.getPubYear() + ", (id = " + book.getId() + ")");
		
		Publisher publisher = book.getPublisher();
		if(publisher !=null)
		{
			printPublisher(publisher);
		}

		List<Author> authors = book.getAuthors();
		for (Author author : authors) {
			printAuthor(author);
		}
		
		output.println();
	}
	
	public void printAllBooks(Collection<Book> books) {
		for (Book b : books) {
			printBook(b);
		}		
	}
	
	
	
	// Printing Author
	public void printAuthor(Author author) {
		output.println(author.getFirstName() + " " + author.getLastName() + " (id = " + author.getId() + ") ");
	}
	
	public void printAllAuthors(Collection<Author> authors) {
		for (Author a : authors) {
			printAuthor(a);
		}		
	}
	
	//Printing Publisher
	public void printPublisher(Publisher publisher)
	{
		output.println(publisher.getName() + " (id = " + publisher.getId() + ")");
	}
	
	public void printAllPublishers(Collection<Publisher> publishers)
	{

		for(Publisher publisher : publishers)
		{
			printPublisher(publisher);
		}
	}
	
	//Printing Customer
	public void printCustomer(Customer cust)
	{
		output.println(cust.getFirstName() + " " + cust.getLastName()+ " nick: " + cust.getAccount().getUsername() + " (id = " + cust.getId() + ")");
	}
	
	public void printAllCustomers(Collection<Customer> custs)
	{
		for(Customer cust : custs)
		{
			printCustomer(cust);
		}
	}
}
