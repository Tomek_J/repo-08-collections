package pl.air.bookstore.service;

import pl.air.bookstore.database.BookstoreDatabase;
import pl.air.bookstore.model.Author;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.model.Customer;
import pl.air.bookstore.model.Order;
import pl.air.bookstore.model.OrderLine;
import pl.air.bookstore.model.Publisher;

public class DataService {
	
	private BookstoreDatabase db;
	
	public DataService(BookstoreDatabase db)
	{
		this.db = db;
	}
	
	private Book createBook(String title, Publisher publisher, int pubYear, Author... authors)
	{
		Book book = new Book(title, pubYear);
		
		for(Author a: authors)
		{
			book.addAuthor(a);	
		}
		
		book.setPublisher(publisher);
		db.addBook(book);
		return book;
	}

	public void createData() {
		
		Publisher wsip = new Publisher("Wydawnictwa Szkolne i Pedagogiczne");
		Publisher nk = new Publisher("Nasza ksi�garnia");
		Publisher wnt  = new Publisher("Wydawnictwa Naukowo-Techiczne");
		
		// Fredro --> Zemsta, �luby Panie�skie
		Author fredro = new Author("Aleksander", "Fredro");
		
		Book zemsta = createBook("Zemsta", wsip, 2012, fredro);
		Book sluby = createBook("�luby panie�skie", nk, 1984, fredro);

		
		// Wrycza, Marcinkowski, Wyrzykowski --> J�zyk UML 2.0 w modelowaniu systemów informatycznych
		Author wrycza = new Author("Stanis�aw", "Wrycza");
		Author marcinkowski = new Author("Boles�aw", "Marcinkowski");
		Author wyrzykowski = new Author("Karol", "Wyrzykowski");
		
		Book uml = createBook("J�zyk UML 2.0 w modelowaniu system�w informatycznych", wnt, 2009, wrycza, marcinkowski, wyrzykowski);
		
		
		// ******************* Homework - start *******************

		// Customer --> Ewa Nowak
		//Customer ewa = new Customer("Ewa", "Nowak");
		
		Customer ewa = new Customer("Ewa", "Nowak", "ewka", "ewka123");     // create a customer together with her account		
		db.addCustomer(ewa);
		
		Customer kasia = new Customer("Kasia", "Niekrasz", "kasia", "gwiazda098");     // create a customer together with her account
		db.addCustomer(kasia);
		
		Customer jerzy = new Customer("Jerzy", "Krawiec", "krawiec", "guziczek");     // create a customer together with his account
		db.addCustomer(jerzy);

		db.addCustomer(new Customer("Marek", "Piekarz", "marek", "joda1980"));     // create a customer and immediately pass it as a parameter for db.addCustomer
		
		// ******************* Homework - end *******************
		
		
		// Order --> placed by Ewa
		Order ewaOrder = new Order("234.04.04.2017", ewa);
		
		OrderLine line1 = new OrderLine(1, zemsta, 100);
		OrderLine line2 = new OrderLine(3, sluby, 50);
		OrderLine line3 = new OrderLine(3, uml, 2);
		
		ewaOrder.addOrderLine(line1);
		ewaOrder.addOrderLine(line2);
		ewaOrder.addOrderLine(line3);
		
		// ******************* Homework - start *******************
		db.addOrder(ewaOrder);
		
				
		// Order --> placed by Kasia
		Order kasiaOrder = new Order("235.18.04.2017", kasia);
		kasiaOrder.addOrderLine(new OrderLine(1, sluby, 1));     // create an order line and immediately pass it as a parameter for kasiaOrder.addOrderLine
		kasiaOrder.addOrderLine(new OrderLine(2, zemsta, 1));
		db.addOrder(kasiaOrder);

		// Order --> placed by Jerzy
		Order jerzyOrder = new Order("236.18.04.2017", jerzy);
		jerzyOrder.addOrderLine(new OrderLine(1, uml, 1));
		db.addOrder(jerzyOrder);
		
		// Order --> one more order placed by Kasia
		Order kasiaOrder2 = new Order("237.18.04.2017", kasia);
		kasiaOrder2.addOrderLine(new OrderLine(1, uml, 5));
		kasiaOrder2.addOrderLine(new OrderLine(2, zemsta, 1));
		kasiaOrder2.addOrderLine(new OrderLine(3, sluby, 1));
		db.addOrder(kasiaOrder2);
		
		// ******************* Homework - end *******************

		
		
		// Print Ewa's order
		//printSrv.printOrder(ewaOrder);
	}
	
	
}
