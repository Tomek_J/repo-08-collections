package pl.air.bookstore;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Collection;

import pl.air.bookstore.database.BookstoreDatabase;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.service.DataService;
import pl.air.bookstore.service.MenuService;
import pl.air.bookstore.service.PrintService;

public class Main {

	public static void main(String[] args) {
		PrintStream out = System.out; //console
		InputStream in = System.in; //keyboard
		
		PrintService printSrv = new PrintService(out);
		BookstoreDatabase db = new BookstoreDatabase();
		
		DataService data = new DataService(db);
		data.createData();
		
		MenuService menu = new MenuService(out, in, printSrv, db);
		
		menu.run();
	}
	/*	public static void main(String[] args) throws FileNotFoundException {		
		//Main m = new Main();
		//m.createDate();
		

		// PrintStream --> console
		PrintStream console = System.out;
		PrintService printSrvConsole = new PrintService(console);
		
		// PrintStream --> file
		PrintStream file = new PrintStream("c:/temp/bookstore.txt");
		PrintService printSrvFile = new PrintService(file);
		
		
		DataService dataSrv = new DataService();		  // create DataService
		BookstoreDatabase db = new BookstoreDatabase();	  // create BookstoreDatabase
		dataSrv.createData(db);                           // create exemplary data and save it to the "db" object (the "db" object is an instance of the BookstoreDatabase class)
		
		
		Book zemsta = db.getBook("Zemsta");          // get a book titled "Zemsta" from the "db" BookstoreDatabase 
		printSrvConsole.printBook(zemsta);           // print "Zemsta" in the console
		printSrvFile.printBook(zemsta);              // print "Zemsta" to the file
		
		
		Collection<Book> books = db.getAllBooks();   // get all books form the "db" BookstoreDatabase
		printSrvConsole.printAllBooks(books);        // print all books in the console
		
		
		printSrvFile.printAllAuthors(db.getAllAuthors());   // print all authors to the file
		
		
		
		// ******************* Homework - start *******************
		
		printSrvConsole.printAllOrders(db.getAllOrders());     // print all orders in the console
		printSrvFile.printAllOrders(db.getAllOrders());        // print all orders to the file
		
		// ******************* Homework - end *******************

		//dodac metody do dodawania zamowien jak ksiazki w dataservice
		//dodac wiecej autorow po id w addbookcmd (authorId...)
		//dodac id do wydawnictwa (2 hashmapy, jedna po nazwie, druga po id) w bookstore
		//book tez maja miec id	i hashmapa po id book
		 * 
		 *zeby dzialal comparator nalezy stworzyc klase bazowa z id i name i reszta po niej dziedziczy i do niej jest comparator
			
	}
*/
	
	
	
}
