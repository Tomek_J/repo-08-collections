package pl.air.bookstore.database;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.air.bookstore.model.Author;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.model.Customer;
import pl.air.bookstore.model.Order;
import pl.air.bookstore.model.Publisher;

public class BookstoreDatabase {

	private Map<String, Book> bookRepo = new HashMap<>();
	private Map<Long, Book>bookIdRepo = new HashMap<>();
	
	private Map<String, Author> authorRepo = new HashMap<>();
	private Map<Long, Author> authorIdRepo = new HashMap<>();
	

	private Map<String, Order> orderRepo = new HashMap<>();
	
	private Map<String, Publisher> publisherRepo = new HashMap<>();
	private Map<Long, Publisher> publisherIdRepo = new HashMap<>();
	
	private Map<String, Customer> customerRepo = new HashMap<>();
	private Map<Long, Customer> customerIdRepo = new HashMap<>();
	
	// ******************* Book *******************
	public void addBook(Book book) {
		bookRepo.put(book.getTitle(), book);
		
		List<Author> authors = book.getAuthors();
		for (Author a : authors) {
			addAuthor(a);
		}
		bookIdRepo.put(book.getId(), book);
		
		Publisher publisher = book.getPublisher();
		if(publisher != null)
		{
			addPublisher(publisher);
		}
	}

	public Book getBook(String title) {
		return bookRepo.get(title);
	}
	
	public Book getBook(long id)
	{
		return bookIdRepo.get(id);
	}
	
	public Collection<Book> getAllBooks() {
		return bookRepo.values();
	}
	
	
	// ******************* Author *******************
	public void addAuthor(Author a) {
		authorRepo.put(a.getFirstName() + a.getLastName(), a);
		authorIdRepo.put(a.getId(), a);
	}
	
	public Author getAuthor(long id)
	{
		return authorIdRepo.get(id);
	}
	public Author getAuthor(String firstName, String lastName)
	{
		return authorRepo.get(firstName + lastName);
	}

	public Collection<Author> getAllAuthors() {
		return authorRepo.values();
	}
	
	
	// ******************* Homework - start *******************
	
	// ******************* Customer *******************
	public void addCustomer(Customer customer) {
		String username = customer.getAccount().getUsername();
		customerRepo.put(username, customer);
		
		long id = customer.getId();
		customerIdRepo.put(id, customer);
	}
	
	public Customer getCustomer(String username) {
		return customerRepo.get(username);
	}
	
	public Customer getCustomer(long id)
	{
		return customerIdRepo.get(id);
	}
	public Collection<Customer> getAllCustomers() {
		return customerRepo.values();
	}

	
	// ******************* Order *******************
	public void addOrder(Order order) {
		orderRepo.put(order.getOrderNo(), order);
	}
	
	public Order getOrder(String orderNo) {
		return orderRepo.get(orderNo);
	}
	
	public Collection<Order> getAllOrders() {
		return orderRepo.values();
	}
	
	
	// ******************* Homework - end *******************

	// ******************* Publisher *******************
	
	public void addPublisher(Publisher publisher)
	{
		publisherRepo.put(publisher.getName(), publisher);
		publisherIdRepo.put(publisher.getId(), publisher);
	}
	
	public Publisher getPublisher(String name)
	{
		return publisherRepo.get(name);
	}
	
	public Publisher getPublisher(long id)
	{
		return publisherIdRepo.get(id);
	}
	
	public Collection<Publisher> getAllPublishers()
	{
		return publisherRepo.values();
	}
}
