package pl.air.bookstore.model;


public class Publisher {
	
    private String name;
    private static long idPublishers;
    private long id;

    
    public Publisher() {
    }

    public Publisher(String name) {
    	this.idPublishers++;
    	this.id = idPublishers;
    	this.name = name;
    }

    
    public String getName() {
        return this.name;
    }
    public void setName(String value) {
        this.name = value;
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return name;
	}
    
}
