package pl.air.bookstore.model;


public class Author {
	
	private static long idGenerator = 0;
	private long id;
    private String firstName;
    private String lastName;

    
    public Author() {
    }
    
    public Author(String firstName, String lastName) {
    	this.idGenerator++;
    	this.id = idGenerator;
    	this.firstName = firstName;
    	this.lastName = lastName;
    }

    public long getId()
    {
    	return this.id;
    }
    
    
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String value) {
        this.lastName = value;
    }


	@Override
	public String toString() {
		return firstName + " " + lastName;
	}
    
}
