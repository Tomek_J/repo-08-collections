package pl.air.bookstore.model;

import java.util.Comparator;

public class OrderLineComparator implements Comparator<OrderLine> {

	@Override
	public int compare(OrderLine o1, OrderLine o2) {
		if (o1.getSeqNo() == o2.getSeqNo())
			return 0;
		if (o1.getSeqNo() > o2.getSeqNo())
			return 1;
		else
			return -1;
	}

}
