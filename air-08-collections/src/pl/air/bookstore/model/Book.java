package pl.air.bookstore.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Book {

	//private static final int BOOK_AUTHOR_NUMBER = 20;
	private static long idBooks;
	private long id;
	
	private String title;
    private int pubYear;
    private Publisher publisher;
    private BigDecimal price;
    
    //private Author[] authors = new Author[BOOK_AUTHOR_NUMBER];
    //private int authorIdx = 0;
    
    private List<Author> authors = new ArrayList<>();

    
    // -------------------- Constructors --------------------  
    public Book() {
    }
    
	public Book(String title, int pubYear) {
		this.idBooks++;
		this.id = idBooks;
    	this.title = title;
    	this.pubYear = pubYear;
    }
	// ------------------------------------------------------
    

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String value) {
        this.title = value;
    }

    public int getPubYear() {
        return this.pubYear;
    }
    public void setPubYear(int value) {
        this.pubYear = value;
    }

    public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

    public BigDecimal getPrice() {
        return this.price;
    }
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    public /*Author[]*/ List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(/*Author[]*/ List<Author> authors) {
		this.authors = authors;
	}
	
    public void addAuthor(Author author) {
    	authors.add(author);
    	/*if (authorIdx == BOOK_AUTHOR_NUMBER) {
    		return;
    	}
    	authors[authorIdx] = author;
    	authorIdx++;*/
    }
    
    /*public int getAuthorIdx() {
    	return this.authorIdx;
    }*/
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return title + (pubYear > 0 ? ", " + pubYear : "");
	}
    
}
